defmodule Starter do
  @moduledoc """
  Documentation for Starter.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Starter.hello
      :world

  """
  def hello do
    :world
  end
end
